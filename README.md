# react-languages-select

[![npm package][npm-badge]][npm]

[//]: # (     [![Travis][build-badge]][build] )
[//]: # (     [![Coveralls][coveralls-badge]][coveralls] )

A customizable language select components for React Js.

This work is derived from [https://ekwonye-richard.github.io/react-languages-select/](https://ekwonye-richard.github.io/react-languages-select/).

## Demo and Example
Live demo: [marcosolari.gitlab.io/react-languages-select/](https://marcosolari.gitlab.io/react-languages-select/)

## Installation
The package can be installed via NPM:
```
npm install react-languages-select --save
```
react-languages-select can be imported as follows

```javascript
import ReactLanguageSelect from 'react-languages-select';

//import css module
import 'react-languages-select/css/react-languages-select.css';

//OR import sass module
import 'react-languages-select/scss/react-languages-select.scss';

```

## Usage

```javascript
    <ReactLanguageSelect />
```

All language Codes: [Language Codes](https://gitlab.com/marcosolari/react-languages-select/-/raw/master/src/languages.js)

### Default Language

You can select a default language to be rendered.

```javascript
    <ReactLanguageSelect
    defaultLanguage="en" />
```

### Searchable

You can enable search filter using prop `searchable`.

```javascript
    <ReactLanguageSelect
    searchable={true} />
```

### Search Placeholder

You can set the placeholder text for search using prop `searchPlaceholder`.

```javascript
    <ReactLanguageSelect
    searchable={true}
    searchPlaceholder="Search for a language" />
```


### Languages

You can use an array of countries rather than the full list of languages.

```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
```
or create a black list of countries

```javascript
    <ReactLanguageSelect
    languages={["ar", "ch"]} />
    blackList={true} />
```

### Local languages names or English languages names or both

You can choose to show the local language names, the international languages names or both.  
The default value is "local".  

To show local language names:
```javascript
    <ReactLanguageSelect
    names={"local"]} />
```
To show international language names:
```javascript
    <ReactLanguageSelect
    names={"international"]} />
```
To show both (local and international) language names:
```javascript
    <ReactLanguageSelect
    names={"both"]} />
```

### Custom Labels

You can use an object of languages labels to replace the languages name. The default language name for a language will be used when the language code has no label passed.

```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
    customLabels={{"en": "EN-US", "fr": "FR", "de": "DE", "it": "IT"}} />
```

### Placeholder

You can replace the default placeholder text.

```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
    customLabels={{"en": "EN-US", "fr": "FR", "de": "DE", "it": "IT"}} />
    placeholder="Select Language" />
```

### Show Selected Label

You can hide or show the label of a selected flag. The default value is true.

```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
    customLabels={{"en": "EN-US", "fr": "FR", "de": "DE", "it": "IT"}} />
    placeholder="Select Language"
    showSelectedLabel={false} />
```

### Show Option Label

You can hide or show the label of the flags in the options dropdown. The default value is true.

```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
    customLabels={{"en": "EN-US", "fr": "FR", "de": "DE", "it": "IT"}} />
    placeholder="Select Language"
    showSelectedLabel={false}
    showOptionLabel={false} />
```

### Selected Size

You can set the size in pixels for the svg flag and label of the selected option.

```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
    customLabels={{"en": "EN-US", "fr": "FR", "de": "DE", "it": "IT"}} />
    placeholder="Select Language"
    showSelectedLabel={false}
    showOptionLabel={false}
    selectedSize={14} />
```

### Options Size

You can set the size in pixels for the svg flags and labels in the options dropdown.
```javascript
    <ReactLanguageSelect
    languages={["en", "fr", "de", "it", "es"]} />
    customLabels={{"en": "EN-US", "fr": "FR", "de": "DE", "it": "IT"}} />
    placeholder="Select Language"
    showSelectedLabel={false}
    showOptionLabel={false}
    selectedSize={18}
    optionsSize={14} />
```

### className

You can pass a style className that will be attached to the top-level `div` of the component.

```javascript
    <ReactLanguageSelect
    className="menu-languages" />
```

### Align Options

You can align the options dropdown to either left or right. The default value is right.

```javascript
    <ReactLanguageSelect
    alignOptions="left" />
```

### Disabled

You can disable the options dropdown, however the selected language can be updated using the `updateSelected()` method. This can be used for language badges.

```javascript
    <ReactLanguageSelect
    defaultLanguage="en"
    showSelectedLabel={false}
    disabled={true} />
```

### onSelect

You can use onSelect event handler which fires each time an option is selected.
`onSelect(languageCode)`.

```javascript
    //onSelect Method
    onSelectLanguage(languageCode){
        console.log(languageCode)
    }
    
    //component render
    <ReactLanguageSelect
    defaultLanguage="en"
    onSelect={this.onSelectLanguage} />
```

### updateSelected

You can dynamically update the selected language on the component using `updateSelected()` method.

```javascript
    //updateSelected Method
    this.refs.userLanguage.updateSelected("it")
    
    //component render
    <ReactLanguageSelect
    ref="userLanguage"
    defaultLanguage="en" />
```

## License
MIT Licensed. Copyright (c) Marco Solari 2020.

[build-badge]: https://img.shields.io/travis/marcosolari/react-languages-select/master.svg?style=flat-square
[build]: https://travis-ci.org/marcosolari/react-languages-select

[npm-badge]: https://img.shields.io/npm/v/react-languages-select.svg?style=flat-square
[npm]: https://www.npmjs.org/package/react-languages-select

[coveralls-badge]: https://img.shields.io/coveralls/marcosolari/react-languages-select/master.svg?style=flat-square
[coveralls]: https://coveralls.io/gitlab/marcosolari/react-languages-select

[coveralls-badge]: https://img.shields.io/coveralls/ekwonye-richard/react-flags-select/master.svg?style=flat-square
[coveralls]: https://coveralls.io/github/ekwonye-richard/react-flags-select