import React from 'react'
import { render } from 'react-dom'
import * as Highlight from 'react-syntax-highlight';
import './scss/demo.scss';
import '../../scss/react-languages-select.scss';
import 'highlight.js/styles/default.css';

import ReactLanguagesSelect from '../../src';

class Demo extends React.Component {
	render() {
		return (
			<div>
				<div className="header">
					<span className="title">React Languages Select</span>
				</div>
				<div className="main">
					<p className="info">A customizable svg languages select components for React Js.</p>
					<div className="section-header">
						<span>Examples</span> 
					</div>
					<hr />
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Default</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect />'} />
						</div>
						<ReactLanguagesSelect />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Default Language</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n defaultLanguage="it" />'} />
						</div>
						<ReactLanguagesSelect
					    defaultLanguage="en" />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Searchable</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n searchable={true} />'} />
						</div>
						<ReactLanguagesSelect
						searchable={true} />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Searchable (with placeholder)</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n searchable={true} \n searchPlaceholder="Search for a language" />'} />
						</div>
						<ReactLanguagesSelect
						searchable={true}
						searchPlaceholder="Search for a language" />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Languages</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr", "de"]} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr", "de"]} />
					</div>

					<div className="demo-group">
						<div className="demo-group-title">
							<span>Languages (BlackList)</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr", "de"]} \n blackList={true} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr", "de"]}
					    blackList={true} />
					</div>

					<div className="demo-group">
						<div className="demo-group-title">
							<span>Names</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr", "ar"]} \n names={"local"} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr", "ar"]}
					    names={"local"} />
					</div>

					<div className="demo-group">
						<div className="demo-group-title">
							<span>Custom Labels</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr"]} \n customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr"]} 
					    customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} />
					</div>

					<div className="demo-group">
						<div className="demo-group-title">
							<span>Placeholder</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr"]} \n customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} \n placeholder="Select Language" />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr"]} 
					    customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}}
					    placeholder="Select Language" />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Show Selected Label</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr"]} \n customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} \n placeholder="Select your preferred language!" \n showSelectedLabel={false} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr"]} 
					    customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}}
					    placeholder="Select your preferred language!"
					    showSelectedLabel={false} />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Show Option Label</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr"]} \n customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} \n placeholder="Select Language" \n showSelectedLabel={false} \n showOptionLabel={false} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr"]} 
					    customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}}
					    placeholder="Select Language"
					    showSelectedLabel={false}
					    showOptionLabel={false} />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Selected Size</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr"]} \n customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} \n placeholder="Select Language" \n showSelectedLabel={false} \n showOptionLabel={false} \n selectedSize={14} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr"]} 
					    customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}}
					    placeholder="Select Language"
					    showSelectedLabel={false}
					    showOptionLabel={false}
					    selectedSize={14} />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Options Size</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n languages={["en", "it", "fr"]} \n customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}} \n placeholder="Select Language" \n showSelectedLabel={false} \n showOptionLabel={false} \n selectedSize={14} \n optionsSize={14} />'} />
						</div>
						<ReactLanguagesSelect
					    languages={["en", "it", "fr"]} 
					    customLabels={{"en": "Eng","it": "Ita","fr": "Fra"}}
					    placeholder="Select Language"
					    showSelectedLabel={false}
					    showOptionLabel={false}
					    selectedSize={14}
					    optionsSize={14} />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>className</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n className="menu-languages" />'} />
						</div>
						<ReactLanguagesSelect
					    className="menu-languages" />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Align Options</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n alignOptions="left" />'} />
						</div>
						<ReactLanguagesSelect
					    alignOptions="left" />
					</div>
					<div className="demo-group">
						<div className="demo-group-title">
							<span>Disabled</span>
						</div>
						<div className="demo-source">
							<Highlight lang={'js'} value={'<ReactLanguagesSelect \n defaultLanguage="en" \n showSelectedLabel={false} \n disabled={true}/>'} />
						</div>
						<ReactLanguagesSelect
					    defaultLanguage="en"
					    showSelectedLabel={false}
					    disabled={true} />
					</div>
				</div>
			</div>
		)
	}
}



render(<Demo/>, document.querySelector('#demo'));
